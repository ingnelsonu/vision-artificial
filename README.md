# Vision Artificial

*************************************************************************
****************Se deben instalar las siguientes librerias **************
*************************************************************************
1. OCR
https://digi.bib.uni-mannheim.de/tesseract/tesseract-ocr-w64-setup-v4.1.0-bibtag19.exe
2. imutils
conda install -c pjamesjoyce imutils
3. pytesseract
conda install -c phygbu pytesseract
4. OpenCV para Python
pip install msgpack
pip install opencv-python
pip install opencv-contrib-python
pip install opencv-python --upgrade
reiniciar PC
5. Descargar el DS de caracteres de https://raw.githubusercontent.com/oyyd/frozen_east_text_detection.pb/master/frozen_east_text_detection.pb 
   y pegarlo en ra carpeta raiz del proyecto Proyecto Billetes COP - Final justo donde esta el archivo "Procesameinto Billetes.ipynb"